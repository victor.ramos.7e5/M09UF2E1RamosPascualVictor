﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    class Program
    {
        static void Main()
        {
            //Exercise1();
            Exercise2();
        }

        static void Exercise1()
        {
            string toWrite1 = "Yo no digo nada ";
            string toWrite2 = "pero... el menda 2 - cremita 0";
            string toWrite3 = "por si lees esto es broma poma :)";

            Thread t1 = new Thread(() => WriteAndWaitOnSpace(toWrite1)),

            t2 = new Thread(() =>
            {
                t1.Join();
                WriteAndWaitOnSpace(toWrite2);
            }),

            t3 = new Thread(() =>
            {
                t2.Join();
                WriteAndWaitOnSpace(toWrite3);
            });

            t1.Start();
            t2.Start();
            t3.Start();
        }

        static void WriteAndWaitOnSpace(string toWrite)
        {
            for (int i = 0; i < toWrite.Length; i++)
            {
                Console.Write(toWrite[i]);
                if (toWrite[i] == ' ')
                    Thread.Sleep(1000);
            }

            Thread.Sleep(1000);
            Console.WriteLine();
        }

        static void Exercise2()
        {
            Nevera nevera = new Nevera(0);

            Thread thread1 = new Thread(() => nevera.LlenarNevera("Anitta"));
            Thread thread2 = new Thread(() => nevera.BeberCerveza("Bad Bunny"));
            Thread thread3 = new Thread(() => nevera.BeberCerveza("Lil Nas X"));
            Thread thread4 = new Thread(() => nevera.BeberCerveza("Manuel Turizo"));

            thread1.Start();
            thread1.Join();

            thread2.Start();
            thread3.Start();
            thread4.Start();
            Console.ReadLine();
        }
    }

}

