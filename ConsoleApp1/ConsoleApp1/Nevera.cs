﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Threads
{
    class Nevera
    {
        public int cantidadCervezas;

        public Nevera(int cantidadCervezas)
        {
            if (cantidadCervezas > 9)
            {
                Console.WriteLine("La nevera no puede tener más de 9 cervezas.");
                this.cantidadCervezas = 9;
            }
            else
            {
                this.cantidadCervezas = cantidadCervezas;
            }
            Console.WriteLine("La nevera se ha llenado con " + this.cantidadCervezas + " cervezas.");
        }

        public void BeberCerveza(string nombre)
        {
            if (cantidadCervezas == 0)
            {
                Console.WriteLine(nombre + " no puede beber porque la nevera está vacía.");
                return;
            }
            int cantidad = new Random().Next(1, 7);
            if (cantidad > cantidadCervezas)
            {
                cantidad = cantidadCervezas;
            }
            cantidadCervezas -= cantidad;
            Console.WriteLine(nombre + " ha bebido " + cantidad + " cervezas. Quedan " + cantidadCervezas + " cervezas.");
        }

        public void LlenarNevera(string nombre)
        {
            if (cantidadCervezas == 9)
            {
                Console.WriteLine(nombre + " no puede llenar la nevera porque ya está llena.");
                return;
            }
            int cantidad = new Random().Next(1, 7);
            if (cantidadCervezas + cantidad > 9)
            {
                cantidad = 9 - cantidadCervezas;
            }
            cantidadCervezas += cantidad;
            Console.WriteLine(nombre + " ha llenado la nevera con " + cantidad + " cervezas. Quedan " + cantidadCervezas + " cervezas.");
        }
    }
}


